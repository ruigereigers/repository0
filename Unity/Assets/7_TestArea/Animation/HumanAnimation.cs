﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class HumanAnimation
{

	public string name = "empty";
	public float TotalTime = 0f;
	public int Length = 0;

	// UpperBody
	public AnimationData Head = new AnimationData();
	public AnimationData Torso = new AnimationData();
	public AnimationData BackRot = new AnimationData();
	public AnimationData LeftArmUpper = new AnimationData();
	public AnimationData LeftArmLower = new AnimationData();
	public AnimationData RightArmUpper = new AnimationData();
	public AnimationData RightArmLower = new AnimationData();

	// LowerBody
	public AnimationData Hips = new AnimationData();
	public AnimationData LeftLegUpper = new AnimationData();
	public AnimationData LeftLegLower = new AnimationData();
	public AnimationData RightLegUpper = new AnimationData();
	public AnimationData RightLegLower = new AnimationData();

	public void CalcLength()
	{
		Length = Head.Length;
	}

	public void Prepare()
	{
		CalcLength();
		CalcTotalTime();
	}

	public void CalcTotalTime()
	{
		TotalTime = 0f;
		for (int i = 0; i < Length; i++)
		{
			TotalTime += Head.Get(i).time;
		}
	}

	public float Time(int index)
	{
		return Head.Get(index).time;
	}
}

