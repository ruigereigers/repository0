﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationLower {

	public AnimVector3 Hips = new AnimVector3();
	public AnimVector3 UpperLegLeft = new AnimVector3();
	public AnimVector3 LowerLegLeft = new AnimVector3();
	public AnimVector3 UpperLegRight = new AnimVector3();
	public AnimVector3 LowerLegRight = new AnimVector3();

	public void Set(Transform[] lowerBody)
	{
		Hips.Set(lowerBody[0].localEulerAngles);
		UpperLegLeft.Set(lowerBody[1].localEulerAngles);
		LowerLegLeft.Set(lowerBody[2].localEulerAngles);
		UpperLegRight.Set(lowerBody[3].localEulerAngles);
		LowerLegRight.Set(lowerBody[4].localEulerAngles);
	}
}
