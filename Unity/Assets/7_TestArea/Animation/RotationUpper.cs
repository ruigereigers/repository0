﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationUpper {

	public AnimVector3 Head = new AnimVector3();
	public AnimVector3 Torso = new AnimVector3();
	public AnimVector3 UpperArmLeft = new AnimVector3();
	public AnimVector3 LowerArmLeft = new AnimVector3();
	public AnimVector3 HandLeft = new AnimVector3();
	public AnimVector3 UpperArmRight = new AnimVector3();
	public AnimVector3 LowerArmRight = new AnimVector3();
	public AnimVector3 HandRight = new AnimVector3();

	public void Set(Transform[] upperBody)
	{
		Head.Set(upperBody[0].localEulerAngles);
		Torso.Set(upperBody[1].localEulerAngles);
		UpperArmLeft.Set(upperBody[2].localEulerAngles);
		LowerArmLeft.Set(upperBody[3].localEulerAngles);
		HandLeft.Set(upperBody[4].localEulerAngles);
		UpperArmRight.Set(upperBody[5].localEulerAngles);
		LowerArmRight.Set(upperBody[6].localEulerAngles);
		HandRight.Set(upperBody[7].localEulerAngles);
	}
}
