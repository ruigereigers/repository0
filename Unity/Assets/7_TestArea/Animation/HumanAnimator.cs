﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HumanAnimator : MonoBehaviour {

	[SerializeField] HumanBody _body;

	[SerializeField] HumanAnimation _drunkWalkAnimation;
	[SerializeField] HumanAnimation _normalWalkAnimation;
	[SerializeField] HumanAnimation _tiredWalkAnimation;

	HumanAnimation _currentAnimation = new HumanAnimation();
	RotationData _fromAnimation = new RotationData();

	float _timer = 0.0f;
	int _stage = 0;

	void Start () {
		_currentAnimation = _drunkWalkAnimation;
		_drunkWalkAnimation.Prepare();
		_normalWalkAnimation.Prepare();
		_tiredWalkAnimation.Prepare();
	}
	
	void Update () {
		DoAnimate();
	}

	void DoAnimate()
	{
		_timer += Time.deltaTime;

		for (int i = 0; i < 10; i++)
		{
			// Check if the animation is finished
			// TODO: Is this needed?
			if (i >= _currentAnimation.Length)
			{
				_stage = 0;

				// Set the fromState
				_fromAnimation.Set(_body.toArray());
				return;
			}
			else if (_stage == i)
			{
				// Morph from last rotation
				float percent = _timer / _currentAnimation.Time(i);

				// Apply the rotations
				_body.Head.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.Head, _currentAnimation.Head.Get(i), percent).ToVector3();
				_body.BackRot.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.BackRot, _currentAnimation.BackRot.Get(i), percent).ToVector3();
				_body.Torso.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.Torso, _currentAnimation.Torso.Get(i), percent).ToVector3();
				_body.LeftArmUpper.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.LeftArmUpper, _currentAnimation.LeftArmUpper.Get(i), percent).ToVector3();
				_body.LeftArmLower.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.LeftArmLower, _currentAnimation.LeftArmLower.Get(i), percent).ToVector3();
				_body.RightArmUpper.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.RightArmUpper, _currentAnimation.RightArmUpper.Get(i), percent).ToVector3();
				_body.RightArmLower.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.RightArmLower, _currentAnimation.RightArmLower.Get(i), percent).ToVector3();

				_body.Hips.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.Hips, _currentAnimation.Hips.Get(i), percent).ToVector3();
				_body.LeftLegUpper.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.LeftLegUpper, _currentAnimation.LeftLegUpper.Get(i), percent).ToVector3();
				_body.LeftLegLower.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.LeftLegLower, _currentAnimation.LeftLegLower.Get(i), percent).ToVector3();
				_body.RightLegUpper.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.RightLegUpper, _currentAnimation.RightLegUpper.Get(i), percent).ToVector3();
				_body.RightLegLower.localEulerAngles = AnimVector3.LerpAngle(_fromAnimation.RightLegLower, _currentAnimation.RightLegLower.Get(i), percent).ToVector3();

				// Check if time is over
				if (percent >= 1.0f)
				{
					_fromAnimation.Set(_body.toArray());
					_timer -= _currentAnimation.Time(i);
					_stage++;

					// Keep _stage in range
					if (_stage >= _currentAnimation.Length)
					{
						_stage = 0;
					}
				}
				return;
			}
		}
	}
}
