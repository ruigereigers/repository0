﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class AnimationData {

	public AnimVector3[] animation = new AnimVector3[0];

	public int Length
	{
		get { return animation.Length; }
	} 

	public AnimVector3 Get(int i)
	{
		if (i >= Length)
		{
			return null;
		}
		return animation[i];
	}
}
