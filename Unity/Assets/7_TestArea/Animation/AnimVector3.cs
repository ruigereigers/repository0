﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// Vector3 class that is serializable and has 
[Serializable]
public class AnimVector3 {

	// Target eulerAngles
	public float x = 0;
	public float y = 0;
	public float z = 0;

	//MorphTime
	public float time = 0;

	public void Set(Vector3 euler, float animTime)
	{
		x = euler.x;
		y = euler.y;
		z = euler.z;
		time = animTime;
	}

	public void Set(Vector3 euler)
	{
		x = euler.x;
		y = euler.y;
		z = euler.z;
		time = 0f;
	}

	public void Set(float newX, float newY, float newZ, float animTime)
	{
		x = newX;
		y = newY;
		z = newZ;
		time = animTime;
	}

	public Vector3 ToVector3()
	{
		Vector3 newVector = new Vector3();
		newVector.x = x;
		newVector.y = y;
		newVector.z = z;
		return newVector;
	}

	public static AnimVector3 LerpAngle(AnimVector3 a, AnimVector3 b, float t)
	{
		AnimVector3 newVector = new AnimVector3();
		newVector.x = Mathf.LerpAngle(a.x, b.x, t);
		newVector.y = Mathf.LerpAngle(a.y, b.y, t);
		newVector.z = Mathf.LerpAngle(a.z, b.z, t);
		return newVector;
	}

	public static AnimVector3 Lerp(AnimVector3 a, AnimVector3 b, float t)
	{
		AnimVector3 newVector = new AnimVector3();
		newVector.x = Mathf.Lerp(a.x, b.x, t);
		newVector.y = Mathf.Lerp(a.y, b.y, t);
		newVector.z = Mathf.Lerp(a.z, b.z, t);
		return newVector;
	}
}
