﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class HumanBody {

	[Header("UpperBody")]
	public Transform Torso;
	public Transform BackRot;
	public Transform Head;
	public Transform LeftArmUpper;
	public Transform LeftArmLower;
	public Transform RightArmUpper;
	public Transform RightArmLower;

	[Header("LowerBody")]
	public Transform Hips;
	public Transform LeftLegUpper;
	public Transform LeftLegLower;
	public Transform RightLegUpper;
	public Transform RightLegLower;

	public Transform[] toArray()
	{
		Transform[] newArray = new Transform[]
		{
			Torso,
			BackRot,
			Head,
			LeftArmUpper,
			LeftArmLower,
			RightArmUpper,
			RightArmLower,
			Hips,
			LeftLegUpper,
			LeftLegLower,
			RightLegUpper,
			RightLegLower,
		};
		return newArray;
	}
}
