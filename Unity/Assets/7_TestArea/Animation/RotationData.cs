﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationData {

	public AnimVector3 Head = new AnimVector3();
	public AnimVector3 Torso = new AnimVector3();
	public AnimVector3 BackRot = new AnimVector3();
	public AnimVector3 LeftArmUpper = new AnimVector3();
	public AnimVector3 LeftArmLower = new AnimVector3();
	public AnimVector3 RightArmUpper = new AnimVector3();
	public AnimVector3 RightArmLower = new AnimVector3();
	public AnimVector3 Hips = new AnimVector3();
	public AnimVector3 LeftLegUpper = new AnimVector3();
	public AnimVector3 LeftLegLower = new AnimVector3();
	public AnimVector3 RightLegUpper = new AnimVector3();
	public AnimVector3 RightLegLower = new AnimVector3();

	public void Set(Transform[] body)
	{
		Torso.Set(body[0].localEulerAngles);
		BackRot.Set(body[1].localEulerAngles);
		Head.Set(body[2].localEulerAngles);
		LeftArmUpper.Set(body[3].localEulerAngles);
		LeftArmLower.Set(body[4].localEulerAngles);
		RightArmUpper.Set(body[5].localEulerAngles);
		RightArmLower.Set(body[6].localEulerAngles);
		Hips.Set(body[7].localEulerAngles);
		LeftLegUpper.Set(body[8].localEulerAngles);
		LeftLegLower.Set(body[9].localEulerAngles);
		RightLegUpper.Set(body[10].localEulerAngles);
		RightLegLower.Set(body[11].localEulerAngles);
	}
}
