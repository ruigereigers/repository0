﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankSpot : MonoBehaviour {

	float beerAmount = 35f;

	public float Drink(float amount)
	{
		if (amount >= beerAmount)
		{
			float toReturn = beerAmount;
			beerAmount = 0;
			return toReturn;
		} else
		{
			beerAmount -= amount;
			return amount;
		}
	}

	public void Reset()
	{
		beerAmount = 35f;
	}

	public bool CheckEmpty()
	{
		if (beerAmount <= 0)
		{
			gameObject.SetActive(false);
			return true;
		}

		return false;
	}
}
