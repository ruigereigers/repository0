﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour {

	PlayerData _pData;

	[SerializeField] Pub[] _pubs;
	[SerializeField] RectTransform _navPointer;
	[SerializeField] RectTransform _navBar;
	[SerializeField] float _gameTime = 300; // seconds

	[SerializeField] Text _finalScoreText;
	[SerializeField] Text _deadCauseText;
	[SerializeField] Text _scoreText;

	AudioSource[] _audioControl;

	[Header("Sounds")]
	[SerializeField] AudioClip _mainMusic;
	[SerializeField] AudioClip _stepSound;
	[SerializeField] AudioClip[] _drinkSound;
	[SerializeField] AudioClip[] _eatSound;
	[SerializeField] AudioClip _continueDrink;

	int _score = 0;

	string _causeText = "The night is over, time to go to internship";

	[SerializeField] Text _timerText;
	float _startTime = 0;

	Pub _currentPub;
	int _currentIndex = 4;
	Vector3 _targetLocation = Vector3.zero;

	bool _finished = false;

	void Awake()
	{
		_audioControl = GetComponents<AudioSource>();
		_pData = GetComponent<PlayerData>();
		_startTime = Time.time;

		SetAudio();
	}

	void SetAudio()
	{
		_audioControl[0].clip = null;
		_audioControl[0].loop = false;
		_audioControl[0].volume = 1f;

		_audioControl[1].clip = _mainMusic;
		_audioControl[1].loop = true;
		_audioControl[1].volume = 0.01f;
		_audioControl[1].Play();

		_audioControl[2].clip = _continueDrink;
		_audioControl[2].loop = true;
		_audioControl[2].volume = 1f;
		ToggleDrink(false);
	}

	void PlayAudio(AudioClip clip)
	{
		_audioControl[0].PlayOneShot(clip);
	}

	void ToggleDrink(bool value)
	{
		if (value && !_audioControl[2].isPlaying)
			_audioControl[2].Play();

		if (!value && _audioControl[2].isPlaying)
		{
			_audioControl[2].Stop();
		}
	}

	void Start()
	{
		NextBar();
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	void Update()
	{
		Timer();
		Navigate();
	}

	void Timer()
	{
		float delta = (_startTime + _gameTime) - Time.time;
		if (delta <= 0)
		{
			if (!_finished)
				PlayerHit();

			if (delta <= -10)
			{
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
				SceneManager.LoadScene(0);
			}
			return;
		}

		int frontInt = Mathf.FloorToInt(delta / 60);
		int backInt = Mathf.FloorToInt(delta % 60);

		string front = frontInt.ToString();
		string back = backInt.ToString();

		if (frontInt < 10)
			front = "0" + front;

		if (backInt < 10)
			back = "0" + back;

		_timerText.text = front + ":" + back;
	}

	void Navigate()
	{
		float maxDisplace = (_navBar.rect.width / 2); //- pixels?
		Vector3 targetPos = _targetLocation;
		targetPos.y = transform.position.y;
		Vector3 dir = targetPos - transform.position;
		float angle = Vector3.Angle(transform.forward, dir);
		Vector3 crossProduct = Vector3.Cross(transform.forward, dir);
		float dotProduct = Vector3.Dot(crossProduct, Vector3.up);

		if (dotProduct < 0)
			angle *= -1;

		Vector3 adaptPos = _navPointer.anchoredPosition;
		adaptPos.x = (angle / 180) * maxDisplace;
		_navPointer.anchoredPosition = adaptPos;
	}

	void AddScore(int amount)
	{
		_score += amount;
		_scoreText.text = _score.ToString();
	}

	public void OnTriggerEnter(Collider col)
	{
		Pickup pickup = col.GetComponent<Pickup>();
		if (pickup != null)
		{
			switch (pickup.Type)
			{
				case TouchType.Beer:
					_pData.AddBeer(10);
					AddScore(Mathf.CeilToInt(10 * (1 + _pData.GetPercent())));
					PlayAudio(_drinkSound[Random.Range(0, _drinkSound.Length)]);
					break;
				case TouchType.Eierbal:
					_pData.AddBeer(-5);
					AddScore(Mathf.CeilToInt(5 * (1 + _pData.GetPercent())));
					PlayAudio(_eatSound[Random.Range(0, _eatSound.Length)]);
					break;
			}
			pickup.PickedUp();
		}
	}

	public void OnTriggerStay(Collider coll)
	{
		TankSpot tankSpot = coll.GetComponent<TankSpot>();
		if (tankSpot != null)
		{
			float drinkSpeed = 5f * Time.deltaTime;
			_pData.AddBeer(tankSpot.Drink(drinkSpeed));

			if (tankSpot.CheckEmpty())
			{
				NextBar();
				AddScore(Mathf.CeilToInt(500 * (1 + _pData.GetPercent())));
				ToggleDrink(false);
				return;
			}
			ToggleDrink(true);
		}
	}

	public void OnCollisionEnter(Collision col)
	{
		Pickup pickup = col.transform.GetComponent<Pickup>();
		if (pickup != null)
		{
			switch (pickup.Type)
			{
				case TouchType.Car:
					_causeText = "You got hit by a car, look left-right-left before you cross a road";
					PlayerHit(col.transform.position);
					return;
			}
		}
	}

	void NextBar()
	{
		int index = Random.Range(0, _pubs.Length);
		while (index == _currentIndex)
		{
			index = Random.Range(0, _pubs.Length);
		}

		_currentIndex = index;
		_currentPub = _pubs[_currentIndex];
		_targetLocation = _currentPub.GetPosition();

		_currentPub.SetAsCurrent();
	}

	void PlayerHit(Vector3 pos)
	{
		if (_finished)
			return;

		GetComponent<PlayerMovement>().enabled = false;
		Rigidbody rigid = GetComponent<Rigidbody>();
		rigid.constraints = RigidbodyConstraints.None;
		Vector3 dir = -(pos - transform.position).normalized;
		rigid.AddForce((Vector3.up + dir) * 150f);
		EndGame();
	}

	void PlayerHit()
	{
		if (_finished)
			return;

		GetComponent<PlayerMovement>().enabled = false;
		Rigidbody rigid = GetComponent<Rigidbody>();
		rigid.constraints = RigidbodyConstraints.None;
		EndGame();
	}

	public void OnBeerFull()
	{
		if (_finished)
			return;

		GetComponent<PlayerMovement>().enabled = false;
		Rigidbody rigid = GetComponent<Rigidbody>();
		rigid.constraints = RigidbodyConstraints.None;
		_causeText = "You passed out, try to maximize your beer consumption by eating snacks";
		EndGame();
	}

	void EndGame()
	{
		_finished = true;

		_timerText.text = "Einde!";
		_startTime = Time.time - _gameTime;

		_finalScoreText.text = "Poar genoom'n: " + _score;
		_deadCauseText.text = _causeText;
		_finalScoreText.transform.parent.gameObject.SetActive(true);
	}
}
