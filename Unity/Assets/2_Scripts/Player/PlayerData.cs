﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData : MonoBehaviour {

	public RectTransform BeerBar;

	[HideInInspector] public float BeerMeterValue = 0f;
	float targetValue = 0f;
	float maxBeerValue = 100f;
	float beerReduction = -0.5f;

	float smoothTime = 0.2f;
	float smoothVelocity;

	void Update () {
		BeerMeter();
		if (Input.GetKey(KeyCode.Space))
		{
			AddBeer(20 * Time.deltaTime);
		}
	}

	void BeerMeter()
	{
		if (BeerMeterValue >= maxBeerValue)
		{
			GetComponent<PlayerManager>().OnBeerFull();
		} else
		{
			BeerMeterValue = Mathf.SmoothDamp(BeerMeterValue, targetValue, ref smoothVelocity, smoothTime);
		}
		AddBeer(beerReduction * Time.deltaTime);

		Mathf.Clamp(BeerMeterValue, 0, maxBeerValue);
		BeerBar.localScale = new Vector3((BeerMeterValue / maxBeerValue), 1, 1);
	}

	public float GetPercent()
	{
		return BeerMeterValue / maxBeerValue;
	}

	public void AddBeer(float amount)
	{
		targetValue = Mathf.Clamp(targetValue + amount, 0f, float.MaxValue);
	}
}
