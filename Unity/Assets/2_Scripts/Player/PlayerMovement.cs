﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMovement : MonoBehaviour
{

	[SerializeField] float _moveSpeed = 10f;
	[SerializeField] Vector2 _mouseSensitivity = new Vector2(50, 25);
	[SerializeField] Transform _playerCamera;
	Rigidbody _rigid;
	PlayerData _pData;
	AudioLowPassFilter _audioPass;

	[SerializeField] Material _blurMaterial;

	[Header("Blur")]
	[SerializeField] AnimationCurve headShakeFactor;
	[SerializeField] AnimationCurve moveSwayFactor;
	[SerializeField] AnimationCurve camDropFactor;
	[SerializeField] AnimationCurve camBlurFactor;
	[SerializeField] AnimationCurve audioPassFactor;

	void Awake()
	{
		_rigid = GetComponent<Rigidbody>();
		_pData = GetComponent<PlayerData>();
		_audioPass = GetComponent<AudioLowPassFilter>();
	}
	
	void Update()
	{
		Movement();
		Camera();

		Audio();
	}

	void Audio()
	{
		float _passFactor = audioPassFactor.Evaluate(_pData.GetPercent());
		_audioPass.cutoffFrequency = _passFactor;
	}

	void Movement()
	{
		// Movement
		float moveZ = Input.GetAxisRaw("Vertical");
		float moveX = Input.GetAxisRaw("Horizontal");

		Vector3 dirZ = transform.forward * moveZ;
		Vector3 dirX = transform.right * moveX;

		Vector3 direction = (dirZ + dirX).normalized;

		if (direction != Vector3.zero)
		{
			float lerpValue = Mathf.PingPong(Time.time, 1);
			Vector3 _swayDir = Vector3.Lerp(transform.right, -transform.right, lerpValue);
			float swayFactor = moveSwayFactor.Evaluate(_pData.GetPercent());
			direction = (direction + _swayDir * swayFactor).normalized;
		}

		direction *= _moveSpeed;


		direction.y = _rigid.velocity.y;
		_rigid.velocity = direction;
	}

	void Camera()
	{
		// Rotation
		float mouseY = Input.GetAxis("Mouse Y");
		float mouseX = Input.GetAxis("Mouse X");

		Vector3 playerEuler = transform.localEulerAngles;
		playerEuler.y += mouseX * _mouseSensitivity.x * Time.deltaTime;

		// Apply headShake
		float shakeFactor = headShakeFactor.Evaluate(_pData.GetPercent());
		float headShake = Mathf.PerlinNoise(Time.time, 0);
		headShake *= shakeFactor;
		headShake -= shakeFactor * 0.5f;

		playerEuler.z = headShake;

		_rigid.MoveRotation(Quaternion.Euler(playerEuler));

		// Apply blur
		float blurFactor = camBlurFactor.Evaluate(_pData.GetPercent());
		_blurMaterial.SetFloat("_Size", blurFactor);

		Vector3 cameraEuler = _playerCamera.localEulerAngles;
		cameraEuler.x += mouseY * _mouseSensitivity.y * Time.deltaTime * -1; //Inverse

		float dropFactor = camDropFactor.Evaluate(_pData.GetPercent());
		cameraEuler.x += dropFactor * Time.deltaTime;
		// Clamp vertical camera values
		if (cameraEuler.x > 180 && cameraEuler.x < 270)
			cameraEuler.x = 270;
		else if (cameraEuler.x < 180 && cameraEuler.x > 90)
			cameraEuler.x = 90;

		_playerCamera.localEulerAngles = cameraEuler;
	}
}
