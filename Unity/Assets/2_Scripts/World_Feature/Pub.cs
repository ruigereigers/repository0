﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pub : MonoBehaviour {

	[SerializeField] TankSpot _drinkPad;

	private void Awake()
	{
		_drinkPad.gameObject.SetActive(false);
	}

	public void SetAsCurrent()
	{
		_drinkPad.gameObject.SetActive(true);
		_drinkPad.Reset();
	}

	public Vector3 GetPosition()
	{
		return _drinkPad.transform.position;
	}
}
