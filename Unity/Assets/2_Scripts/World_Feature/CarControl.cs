﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarControl : MonoBehaviour {

	[SerializeField] Transform _car;
	[SerializeField] Transform _pointParent;

	[SerializeField] float _moveSpeed = 8;

	List<Vector3> _wayPoints = new List<Vector3>();
	int _pointIndex = 0;

	Vector3 rotationVelocity;
	float _minDistance = 1;
	float smoothRotationTime = 0.25f;

	void Start () {
		RegisterPoints();
		_car.position = _wayPoints[0];
		_car.forward = (_wayPoints[1] - _wayPoints[0]).normalized;
	}
	
	void Update () {
		Move();
	}

	void RegisterPoints()
	{
		foreach (Transform child in _pointParent)
		{
			Vector3 newPoint = child.position;
			newPoint.y = 0;
			_wayPoints.Add(newPoint);
		}
	}

	void Move()
	{
		// Rotation
		Vector3 dir = (_wayPoints[_pointIndex] - _car.position);
		float distance = dir.magnitude;
		dir.Normalize();

		_car.forward = Vector3.SmoothDamp(_car.forward, dir, ref rotationVelocity, smoothRotationTime);

		//_car.forward = dir;

		// Movement
		_car.position = _car.position + _car.forward * _moveSpeed * Time.deltaTime;

		if (distance <= _minDistance)
		{
			_pointIndex++;
			if (_pointIndex >= _wayPoints.Count)
				_pointIndex = _pointIndex % _wayPoints.Count;
		}
	}
}
