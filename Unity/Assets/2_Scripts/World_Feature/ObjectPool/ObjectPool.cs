﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool {

	List<Pickup> _allObjects;
	List<Pickup> _available = new List<Pickup>();
	List<Pickup> _unavailable = new List<Pickup>();

	public ObjectPool(GameObject prefab, int count, Transform parent)
	{
		_allObjects = new List<Pickup>(count);

		for (int i = 0; i < count; i++)
		{
			GameObject newObject = GameObject.Instantiate(prefab, parent);
			newObject.SetActive(false);

			Pickup newPickup = newObject.GetComponent<Pickup>();
			newPickup.Register(this);

			_allObjects.Add(newPickup);
			_available.Add(newPickup);
		}
	}

	public Pickup GetObject()
	{
		if (_available.Count <= 0)
			return null;
		Pickup first = _available[0];
		_available.Remove(first);
		_unavailable.Add(first);

		first.gameObject.SetActive(true);
		return first;
	}

	public void ReturnObject(Pickup toReturn)
	{
		if (toReturn == null)
			return;

		if (_allObjects.Contains(toReturn))
		{
			toReturn.gameObject.SetActive(false);
			_unavailable.Remove(toReturn);
			_available.Add(toReturn);
		}
	}
}
