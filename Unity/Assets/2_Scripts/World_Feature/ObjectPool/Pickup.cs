﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TouchType
{
	None,
	Beer,
	Eierbal,
	Car
}

public class Pickup : MonoBehaviour {

	ObjectPool _myPool;

	SpawnPoint _spawnedOn = null;

	public TouchType Type;

	public void Register(ObjectPool myPool)
	{
		_myPool = myPool;
	}

	public void Spawned(SpawnPoint newSpawn)
	{
		_spawnedOn = newSpawn;
		_spawnedOn.Empty = false;
		transform.position = _spawnedOn.Position;
	}

	public void ReturnToPool()
	{
		_myPool.ReturnObject(this);
		_spawnedOn.Empty = true;
		_spawnedOn = null;
	}

	public void PickedUp()
	{
		ReturnToPool();
	}
}
