﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

	[HideInInspector] public bool Empty = true;

	public Vector3 Position { get { return transform.position; } }
}
