﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupSpawner : MonoBehaviour {

	[SerializeField] Transform player;
	[SerializeField] float _spawnDelay = 5f;

	[Header("points")]
	[SerializeField] Transform _pointParent;
	List<SpawnPoint> _spawnPoints = new List<SpawnPoint>();

	[Header("Prefabs")]
	[SerializeField] GameObject _beerPrefab;
	[SerializeField] GameObject _eggPrefab;

	[Header("ObjectHolders")]
	[SerializeField] Transform _beerContainer;
	[SerializeField] Transform _eggContainer;

	ObjectPool _beerPool;
	ObjectPool _eggPool;
	float _spawnTimer = 0.0f;

	void Start ()
	{
		_beerPool = new ObjectPool(_beerPrefab, 30, _beerContainer);
		_eggPool = new ObjectPool(_eggPrefab, 30, _eggContainer);

		RegisterSpawns();

		float startSpawn = 8;
		for (int i = 0; i < startSpawn; i++)
		{
			SpawnObject();
		}
	}
	
	void Update ()
	{
		_spawnTimer += Time.deltaTime;
		
		if (_spawnTimer >= _spawnDelay)
		{
			_spawnTimer -= _spawnDelay;
			SpawnObject();
		}
	}

	void RegisterSpawns()
	{
		foreach(Transform child in _pointParent)
		{
			_spawnPoints.Add(child.GetComponent<SpawnPoint>());
		}
	}

	void SpawnObject()
	{
		ObjectPool getFromPool = _eggPool;
		if (Random.value >= 0.4f)
			getFromPool = _beerPool;

		int index = Random.Range(0, _spawnPoints.Count);
		SpawnPoint check = _spawnPoints[index];
		int counter = 0;

		while (!check.Empty)
		{
			index = Random.Range(0, _spawnPoints.Count);
			check = _spawnPoints[index];
			counter++;
			if (counter > 12)
			{
				Debug.Log("No empty spawns found");
				return;
			}
		}

		Pickup newPickup = getFromPool.GetObject();
		newPickup.Spawned(check);
	}
}
