﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupMover : MonoBehaviour {

	Transform _child;

	float _rotateSpeed = 90f;

	void Start () {
		_child = transform.GetChild(0);
	}
	
	void Update () {
		_child.Rotate(Vector3.up * _rotateSpeed * Time.deltaTime);
		_child.localPosition = new Vector3(0, -Mathf.PingPong(Time.time / 3, 0.3f), 0);
	}
}
