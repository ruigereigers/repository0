﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TestMovement : MonoBehaviour {

	[SerializeField] float _moveSpeed = 5;
	[SerializeField] Vector2 _mouseSensitivity = new Vector2(50, 25);

	[SerializeField] Transform _playerCamera;

	Rigidbody _rigid;

	void Awake()
	{
		_rigid = GetComponent<Rigidbody>();
	}

	void Start()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	void Update ()
	{
		ApplyInput();
	}

	void ApplyInput()
	{
		// Movement
		float moveZ = Input.GetAxisRaw("Vertical");
		float moveX = Input.GetAxisRaw("Horizontal");

		Vector3 dirZ = transform.forward * moveZ;
		Vector3 dirX = transform.right * moveX;

		Vector3 direction = (dirZ + dirX).normalized * _moveSpeed;
		direction.y = _rigid.velocity.y;
		_rigid.velocity = direction;

		// Rotation
		float mouseY = Input.GetAxisRaw("Mouse Y");
		float mouseX = Input.GetAxisRaw("Mouse X");

		Vector3 playerEuler = transform.localEulerAngles;
		playerEuler.y += mouseX * _mouseSensitivity.x * Time.deltaTime;
		_rigid.MoveRotation(Quaternion.Euler(playerEuler));

		Vector3 cameraEuler = _playerCamera.localEulerAngles;
		cameraEuler.x += mouseY * _mouseSensitivity.y * Time.deltaTime * -1; //Inverse
		_playerCamera.localEulerAngles = cameraEuler;
	}

	public void OnTriggerEnter(Collider col)
	{
		Pickup pick = col.GetComponent<Pickup>();

		if (pick != null)
		{
			Debug.Log("Pickup");
			pick.PickedUp();
		}
	}
}
